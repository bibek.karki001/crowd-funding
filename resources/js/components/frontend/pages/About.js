import React from "react";
import Header from "../includes/Header";
import Footer from "../includes/Footer";
import CopyRight from "../includes/CopyRight";
import PageBanner from "../includes/PageBanner";
import Welcome from "../includes/Welcome";
import Doctors from "../includes/Doctors";
import Services from "../includes/Services";

const welcomeContent = {
    title:'Welcome To Our Hospital',
    description: `Lorem ipsum dolor sit amet consectetur adipisicing elit.
    Minus, quo! Minus quasi assumenda incidunt excepturi,
    dolores voluptas aut ipsa, tenetur quae harum, eos sapiente
    dolor laborum nulla cumque ducimus reiciendis.`,
    imageUrl:'../../../../images/about1.jpg',
}

const About = () => {
    return (
        <div>
            <Header />
            <PageBanner />
            <Welcome welcomeContent = {welcomeContent} />
            <Services />
            <Doctors />
            <Footer />
            <CopyRight />
        </div>
    );
};

export default About;
