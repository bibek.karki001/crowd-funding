import React from "react";
import Header from "../includes/Header";
import Footer from "../includes/Footer";
import CopyRight from "../includes/CopyRight";
import Banner from "../includes/Banner";
import Welcome from "../includes/Welcome";
import Department from "../includes/Department";

const welcomeContent = {
    title:'Welcome To Our Hospital',
    description: `Lorem ipsum dolor sit amet consectetur adipisicing elit.
    Minus, quo! Minus quasi assumenda incidunt excepturi,
    dolores voluptas aut ipsa, tenetur quae harum, eos sapiente
    dolor laborum nulla cumque ducimus reiciendis.`,
    imageUrl:'../../../../images/doctors.png',
}

const Home = () => {
    return (
        <div>
            <Header />
            <Banner />
            <Welcome welcomeContent = {welcomeContent} />
            <Department />
            <Footer />
            <CopyRight />
        </div>
    );
};

export default Home;
