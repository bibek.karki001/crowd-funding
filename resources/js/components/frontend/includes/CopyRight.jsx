import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        background: '#1e76c5',
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: "center",
        color: theme.palette.common.white,
        boxShadow: "none",
        background: '#1e76c5',
    }
}));

const CopyRight = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <Typography align='center'>
                            &copy; Copyright 2020 Crowd Funding.
                            All Rights Reserved.
                        </Typography>
                    </Paper>
                </Grid>
            </Grid>
        </div>
    );
};

export default CopyRight;
