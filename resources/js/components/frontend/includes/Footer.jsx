import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import InstagramIcon from "@material-ui/icons/Instagram";

const useStyles = makeStyles(theme => {
    console.log(theme);
    return {
        root: {
            flexGrow: 1,
            background: '#319dfe',
            marginTop: 20,
            padding: "30px 30px"
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: "center",
            color: theme.palette.common.white,
            background: '#319dfe'
        },
        footerSection: {
            textAlign: "left",
            color: "#fff"
        },
        footerLink: {
            display: "block",
            fontSize: 16
        }
    };
});

const Footer = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs>
                    <div className={classes.footerSection}>
                        <Typography variant="h5">Crowd Funding</Typography>
                        <Typography>
                            Lorem ipsum dolor, sit amet consectetur adipisicing
                            elit. Maiores ducimus tempora accusamus totam sint
                            numquam provident enim exercitationem deleniti
                            debitis porro harum nesciunt laudantium nulla
                            delectus laboriosam, excepturi quasi inventore!
                        </Typography>
                    </div>
                </Grid>
                <Grid item xs>
                    <div className={classes.footerSection}>
                        <Typography variant="h5">Follow Us</Typography>
                        <div>
                            <FacebookIcon fontSize="large" />
                            <InstagramIcon fontSize="large" />
                            <TwitterIcon fontSize="large" />
                        </div>
                    </div>
                </Grid>
                <Grid item xs>
                    <div className={classes.footerSection}>
                        <Typography variant="h5">Useful Links</Typography>
                        <Link
                            href="#"
                            className={classes.footerLink}
                            color="inherit"
                        >
                            About Us
                        </Link>
                        <Link
                            href="#"
                            className={classes.footerLink}
                            color="inherit"
                        >
                            Contact Us
                        </Link>
                        <Link
                            href="#"
                            className={classes.footerLink}
                            color="inherit"
                        >
                            Privacy Policy
                        </Link>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
};

export default Footer;
