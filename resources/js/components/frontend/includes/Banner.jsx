import React from "react";
import banner from "../../../../images/image3.jpg";
import { makeStyles } from "@material-ui/core/styles";
import CustomCard from "./CustomCard";

const useStyles = makeStyles(theme => ({
    bannerWrapper: {
        position: "relative"
    },
    bannerImage: {
        width:'100%',
    },
    slogan: {
        left: "30px",
        color: "#1f1f1f",
        top: "15%",
        fontSize:48,
        position: "absolute",
        fontWeight: "bolder",
        textTransform: "capitalize"
    },
    cards: {
        width:'100%',
        padding: '0 30px',
    },
    cardWrapper: {
       marginTop: '-80px',
       display:'flex',
       justifyContent:'space-between',
    },
}));

const Banner = () => {
    const classes = useStyles();
    return (
        <div className={classes.bannerWrapper}>
            <img className={classes.bannerImage} src={banner} alt="banner" />
            <h1 className={classes.slogan}>
                We put our heart into healing yours
            </h1>
            <div className={`${classes.cards}`}>
                <div className={`${classes.cardWrapper}`}>
                    <CustomCard />
                    <CustomCard />
                    <CustomCard />
                </div>
            </div>
        </div>
    );
};

export default Banner;
