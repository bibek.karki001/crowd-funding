import React from "react";
import pageBanner from "../../../../images/page-banner1.png";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
    bannerWrapper: {
        position: "relative"
    },
    bannerImage: {
        width: "100%"
    },
    overLay: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        content: "",
        opacity: 0.6,
        background:
            "linear-gradient(to right,#5db2ff 0%,#65b4f9 24%,rgba(124,185,233,0) 96%,rgba(125,185,232,0) 100%)"
    },
    breadCrum: {
        position: "absolute",
        bottom: "30%",
        left: "15%",
        background: "#319dfe",
        padding: "10px 15px",
        color: "#fff",
        fontWeight: 700
    }
}));

const PageBanner = () => {
    const classes = useStyles();
    return (
        <div className={classes.bannerWrapper}>
            <div className={classes.overLay}></div>
            <img
                className={classes.bannerImage}
                src={pageBanner}
                alt="banner"
            />
            <Typography variant='h5' className={classes.breadCrum}>About Us</Typography>
        </div>
    );
};

export default PageBanner;
