import React from "react";
import { withRouter } from "react-router-dom";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import clsx from "clsx";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import HomeIcon from "@material-ui/icons/Home";
import ContactIcon from "@material-ui/icons/Contacts";
import InfoIcon from "@material-ui/icons/Info";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    header: {
        background: "#319dfe"
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    title: {
        flexGrow: 1
    },
    list: {
        width: 250
    },
    fullList: {
        width: "auto"
    }
}));

const menuItems = [
    { title: "Home", url: "/" },
    { title: "About", url: "about" },
    { title: "Contact", url: "contact" }
];

const Header = props => {
    const classes = useStyles();
    const theme = useTheme();
    const { history } = props;
    const isMobile = useMediaQuery(theme.breakpoints.down("xs"));
    // console.log(props);
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false
    });

    const toggleDrawer = (anchor, open) => event => {
        if (
            event.type === "keydown" &&
            (event.key === "Tab" || event.key === "Shift")
        ) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    const list = anchor => (
        <div
            className={clsx(classes.list, {
                [classes.fullList]: anchor === "top" || anchor === "bottom"
            })}
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
        >
            <List>
                {menuItems.map((menu, index) => (
                    <ListItem
                        button
                        key={menu.url}
                        onClick={() => handleMenuClick(menu.url)}
                    >
                        <ListItemIcon>
                            {menu.title === "Home" ? (
                                <HomeIcon />
                            ) : menu.title === "Contact" ? (
                                <ContactIcon />
                            ) : menu.title === "About" ? (
                                <InfoIcon />
                            ) : (
                                <HomeIcon />
                            )}
                        </ListItemIcon>
                        <ListItemText primary={menu.title} />
                    </ListItem>
                ))}
            </List>
        </div>
    );

    const handleMenuClick = url => {
        history.push(url);
    };

    const drawer = () => (
        <React.Fragment key="left">
            <IconButton
                onClick={toggleDrawer("left", true)}
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="menu"
            >
                <MenuIcon />
            </IconButton>
            <Drawer
                anchor="left"
                open={state["left"]}
                onClose={toggleDrawer("left", false)}
            >
                {list("left")}
            </Drawer>
        </React.Fragment>
    );
    return (
        <div className={classes.root}>
            <AppBar position="static" className={classes.header}>
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        Crowd Funding
                    </Typography>

                    {isMobile ? (
                        drawer()
                    ) : (
                        <>
                            {menuItems.map(menu => {
                                return (
                                    <Button
                                        color="inherit"
                                        key={menu.url}
                                        onClick={() =>
                                            handleMenuClick(menu.url)
                                        }
                                    >
                                        {menu.title}
                                    </Button>
                                );
                            })}
                        </>
                    )}
                </Toolbar>
            </AppBar>
        </div>
    );
};

export default withRouter(Header);
