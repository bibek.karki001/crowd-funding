import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Expert1 from "../../../../images/expert1.webp";
import Expert2 from "../../../../images/expert2.webp";
import Expert3 from "../../../../images/expert3.webp";
import Expert4 from "../../../../images/expert4.webp";
import "../../../../styles/department.scss";

const useStyles = makeStyles({
    root: {
        minWidth: 300,
        marginTop:20,
    },
    media: {
        height: 180
    },
    title: {
        textTransform:'capitalize',
    }
});

const doctors = [
    {
        name: "Dr. Strange",
        imageUrl: Expert1,
        specialist:"surgeon"
    },
    {
        name: "Dr. Subedi",
        imageUrl: Expert2,
        specialist:"surgeon"
    },
    {
        name: "Dr. Dre",
        imageUrl: Expert3,
        specialist:"pediatrist"
    },
    {
        name: "Dr. Strange",
        imageUrl: Expert4,
        specialist:"laboratory"
    },
];

const Department = () => {
    const classes = useStyles();

    return (
        <div className={`depart-container`}>
            <Typography gutterBottom variant="h3" component="h2" align="center">
                Our Doctors
            </Typography>
            <div className="depart-wrapper">
                {doctors.map((doctor, key) => (
                    <Card className={classes.root} key={key}>
                        <CardActionArea>
                            <CardMedia
                                className={classes.media}
                                image={doctor.imageUrl}
                                title="Contemplative Reptile"
                            />
                            <CardContent>
                                <Typography
                                    gutterBottom
                                    variant="h5"
                                    component="h2"
                                    className={classes.title}
                                >
                                    {doctor.name}
                                </Typography>
                                <Typography
                                    variant="body2"
                                    color="textSecondary"
                                    component="p"
                                >
                                    {doctor.specialist}
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                ))}
            </div>
        </div>
    );
};

export default Department;
