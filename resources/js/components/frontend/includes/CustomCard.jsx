import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import AccessAlarmIcon from "@material-ui/icons/AccessAlarm";

const useStyles = makeStyles({
  root: {
    maxWidth: 400,
    background:'#319dfe',
    color:'#fff',
    opacity:0.9,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
      fontWeight:'bold',
      color:'#fff',
  },
  text: {
      color:'#fff',
  },
  pos: {
    marginBottom: 12,
  },
});

export default function SimpleCard() {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <Card className={classes.root}>
      <CardContent>
        <AccessAlarmIcon />
        <Typography variant='h6' className={classes.title} color="textSecondary" gutterBottom>
          Word of the Day
        </Typography>
        <Typography className={classes.text} color="textSecondary">
         Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint nemo asperiores nostrum illo consequuntur commodi autem magni repudiandae. Debitis non molestias ullam officia, sed consequatur nobis nam. Eius, sequi eligendi?
        </Typography>
      </CardContent>
    </Card>
  );
}
