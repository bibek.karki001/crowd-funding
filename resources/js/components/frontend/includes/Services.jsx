import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import PersonIcon from "@material-ui/icons/PersonOutline";
import BusinessIcon from "@material-ui/icons/Business";
import AirportShuttleIcon from "@material-ui/icons/AirportShuttle";
import LocalPharmacyIcon from "@material-ui/icons/LocalPharmacy";
import Icon from "@material-ui/core/Icon";
import "../../../../styles/services.scss";

const useStyles = makeStyles({
    root: {
        maxWidth: 300
    },
    bullet: {
        display: "inline-block",
        margin: "0 2px",
        transform: "scale(0.8)"
    },
    title: {
        fontWeight: 700,
        marginBottom: 15
    },
    pos: {
        marginBottom: 12
    },
    serviceWrapper: {
        padding: 30
    },
    icon: {
        background: "#319dfe",
        color: "#fff",
        width: 60,
        height: 60,
        fontSize: 40,
        paddingTop: 10,
        borderRadius: "50%"
    }
});

const services = [
    {
        title: "Qualified Doctors",
        description: `Lorem ipsum dolor sit, amet consectetur adipisicing
        elit. Omnis ducimus laborum eos facilis rerum odit, qui
        harum necessitatibus sunt, ullam at recusandae aperiam
        asperiores quos. Doloremque exercitationem tenetur
        nostrum reprehenderit?`,
        icon: "person_outline"
    },
    {
        title: "Medical Counceling",
        description: `Lorem ipsum dolor sit, amet consectetur adipisicing
        elit. Omnis ducimus laborum eos facilis rerum odit, qui
        harum necessitatibus sunt, ullam at recusandae aperiam
        asperiores quos. Doloremque exercitationem tenetur
        nostrum reprehenderit?`,
        icon: "business"
    },
    {
        title: "Emergency Services",
        description: `Lorem ipsum dolor sit, amet consectetur adipisicing
        elit. Omnis ducimus laborum eos facilis rerum odit, qui
        harum necessitatibus sunt, ullam at recusandae aperiam
        asperiores quos. Doloremque exercitationem tenetur
        nostrum reprehenderit?`,
        icon: "airport_shuttle"
    },
    {
        title: "Free Medicine",
        description: `Lorem ipsum dolor sit, amet consectetur adipisicing
        elit. Omnis ducimus laborum eos facilis rerum odit, qui
        harum necessitatibus sunt, ullam at recusandae aperiam
        asperiores quos. Doloremque exercitationem tenetur
        nostrum reprehenderit?`,
        icon: "local_pharmacy"
    }
];

const Services = () => {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;

    return (
        <div className={`${classes.serviceWrapper} service-wrapper`}>
            <Typography gutterBottom variant="h3" component="h2" align="center">
                Our Services
            </Typography>
            <div className="services">
                {services.map(service => (
                    <Card
                        className={`${classes.root} service`}
                        align="center"
                        key={service.title}
                    >
                        <CardContent>
                            <Icon className={`${classes.icon} icon`}>
                                {service.icon}
                            </Icon>
                            <Typography
                                variant="h5"
                                className={classes.title}
                                color="textSecondary"
                                gutterBottom
                            >
                                {service.title}
                            </Typography>
                            <Typography color="textSecondary">
                                {service.description}
                            </Typography>
                        </CardContent>
                    </Card>
                ))}
            </div>
        </div>
    );
};

export default Services;
