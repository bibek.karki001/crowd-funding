import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Image1 from "../../../../images/1.webp";
import Image2 from "../../../../images/2.webp";
import Image3 from "../../../../images/3.webp";
import Image4 from "../../../../images/4.webp";
import Image5 from "../../../../images/5.webp";
import Image6 from "../../../../images/6.webp";
import "../../../../styles/department.scss";

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
        marginTop:20,
    },
    media: {
        height: 180
    },
    title: {
        textTransform:'capitalize',
    }
});

const departments = [
    {
        title: "eye care",
        imageUrl: Image1,
        description:
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ratione blanditiis, optio ea itaque quaerat possimus"
    },
    {
        title: "Physical Therapy",
        imageUrl: Image2,
        description:
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ratione blanditiis, optio ea itaque quaerat possimus"
    },
    {
        title: "dental care",
        imageUrl: Image3,
        description:
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ratione blanditiis, optio ea itaque quaerat possimus"
    },
    {
        title: "Diagnostic Test",
        imageUrl: Image4,
        description:
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ratione blanditiis, optio ea itaque quaerat possimus"
    },
    {
        title: "Skin Surgery",
        imageUrl: Image5,
        description:
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ratione blanditiis, optio ea itaque quaerat possimus"
    },
    {
        title: "Surgery Service",
        imageUrl: Image6,
        description:
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ratione blanditiis, optio ea itaque quaerat possimus"
    }
];

const Department = () => {
    const classes = useStyles();

    return (
        <div className={`depart-container`}>
            <Typography gutterBottom variant="h3" component="h2" align="center">
                Our Departments
            </Typography>
            <div className="depart-wrapper">
                {departments.map(department => (
                    <Card className={classes.root} key={department.title}>
                        <CardActionArea>
                            <CardMedia
                                className={classes.media}
                                image={department.imageUrl}
                                title="Contemplative Reptile"
                            />
                            <CardContent>
                                <Typography
                                    gutterBottom
                                    variant="h5"
                                    component="h2"
                                    className={classes.title}
                                >
                                    {department.title}
                                </Typography>
                                <Typography
                                    variant="body2"
                                    color="textSecondary"
                                    component="p"
                                >
                                    {department.description}
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                                Learn More
                            </Button>
                        </CardActions>
                    </Card>
                ))}
            </div>
        </div>
    );
};

export default Department;
