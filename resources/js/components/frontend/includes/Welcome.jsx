import React from "react";
import "../../../../styles/welcome.scss";
import Typography from "@material-ui/core/Typography";

const Welcome = ({welcomeContent}) => {
    return (
        <div className={`welcome-wrapper`}>
            <div className={`welcome-text`}>
                <Typography
                    variant="h6"
                    className={`welcom-title`}
                    color="textSecondary"
                    gutterBottom
                >
                    {welcomeContent.title}
                </Typography>
                <Typography color="textSecondary">
                   {welcomeContent.description}
                </Typography>
            </div>
            <div className={`welcome-image`}>
                <img src={welcomeContent.imageUrl} alt="welcome" />
            </div>
        </div>
    );
};

export default Welcome;
