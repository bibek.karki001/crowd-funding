import React from "react";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import Header from './includes/Header';
import About from "./pages/About";
import Home from "./pages/Home";
import Contact from "./pages/Contact";

const Index = () => {
    return (
        <div>
            <Header />
            <Router>
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/about" exact component={About} />
                    <Route path="/contact" exact component={Contact} />
                </Switch>
            </Router>
        </div>
    );
};

export default Index;
